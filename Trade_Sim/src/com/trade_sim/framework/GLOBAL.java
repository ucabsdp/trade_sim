package com.trade_sim.framework;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Observable;

import javax.swing.text.DateFormatter;

/**
 * Contains the global simulation time settings
 * @author will_palmer
 *
 */
public class GLOBAL {
	public enum TimeFrame{
		YEARLY(1), WEEKLY(52), DAILY(250), INTRADAY(0) ;
		
		private int i;
		private TimeFrame(int i){
			this.i = i;
		}
		
		public int getValue(){
			return i;
		}	
	}
	
	public static class GlobalListener extends Observable{
		public GlobalListener(){}
	}
	
	private static Date currentDate = null;
	private static Date startDate = null;
	private static long t = 0;
	private static int timeStep = 0;
	public static boolean isDate = false;
	public static TimeFrame timeFrame;
	public static GlobalListener listener = new GlobalListener();
	
	public static Date getCurrentTime(){
		return currentDate;
	}
	
	/**
	 * 
	 * @return Current time elapsed, either timesteps or time in milliseconds, depending of the time measure used
	 */
	public static int getTimeElapsed() {
		int timeElapsed = 0;
		if(isDate = false){
			return timeStep;
		}
		Calendar date =  Calendar.getInstance();
		date.setTime(startDate);
		//Timeframe Switch to count
		switch(timeFrame){
		case DAILY :   
			
			while(date.before(currentDate)){
				date.add(Calendar.DAY_OF_MONTH, 1);
				timeElapsed ++;
			}break;
			
		  
		}  
	
		return timeElapsed;
	}
	
	public static void setCurrentSimulationTime(String source, String format){
		DateFormat formatter = new SimpleDateFormat(format);
		try {
			setCurrentSimulationTime(formatter.parse(source));
		} catch (ParseException e) {
			System.out.println("SimulationFrame date failed");
			e.printStackTrace();
		}
	}
	
	public static void setCurrentSimulationTime(Date date){
		if(currentDate == null){
		startDate = date;
		}
		
		currentDate = date;
		isDate = true;
		
		GLOBAL.listener.notifyObservers();
	}
	
	public static void resetDate(){
		currentDate = null;
		startDate = null;
		//timeElapsed = 0;
	}
	
	public static void resetTime(){
		resetDate();
		resetTimeStep();
	}
	
	
	public static int getTimeStep(){
		return timeStep;
	}

	public static void incrementTimeStep(){
			timeStep++;
			GLOBAL.listener.notifyObservers();
	}
	
	public static void resetTimeStep(){
		timeStep=0;
	}

	public static TimeFrame getTimeFrame() {
		return timeFrame;
	}

	public static void setTimeFrame(TimeFrame timeFrame) {
		GLOBAL.timeFrame = timeFrame;
	}
}
