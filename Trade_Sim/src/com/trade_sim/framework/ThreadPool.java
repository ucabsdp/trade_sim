package com.trade_sim.framework;

import java.util.HashMap;


/**
 * The ThreadPool allocates and manages the worker threads used. Specific ThreadGroups can be accessed and 
 * killed. This class cleans up all active threads (if registered to a TheadGroup) on termination.
 * @author Samuel Palmer
 *
 */
public class ThreadPool {
	protected static ThreadGroup main = new ThreadGroup("Main");
	protected static ThreadGroup tg = new ThreadGroup(main,"Log Threads");
	protected static ThreadGroup simulationGroup = new ThreadGroup(main,"Simulations");
	protected static HashMap<String, Log> logThreadMap = new HashMap<String, Log>(10);

	protected Thread t = new Thread(tg, "Log");
	
	public static void registerToPool(Thread t){
	
	}
	
	/** 
	 * Control group for all simulation threads running
	 * @return ThreadGroup for all simulations
	 */
	public static ThreadGroup getSimulationGroup(){
		return simulationGroup;
	}
	/**
	 * kills all running simulations
	 */
	public static void kilRunningSimulation(){
		simulationGroup.interrupt();
	}
	
	/**
	 * 
	 * @return Super group of all the registered worker threads for the program
	 */
	public static ThreadGroup getMainGroup(){
		return main;
	}
	
	public static Scheduler getScheduler(){
		return new Scheduler(main, "Scheduler");
	}
	
	/**
	 * 
	 * @return Log Thread group
	 */
	public static ThreadGroup getRunningGroup(){
		return tg;
	}
	
	public static void killLogs(){
		tg.interrupt();
	}
	
	/**
	 * Instantiates a new log thread for the file or returns an existing thread for the file
	 * @param filename
	 * @return 
	 */
	public static Log getLogThread(String filename){
		if(logThreadMap.containsKey(filename)){
			return logThreadMap.get(filename);
		}else{
			logThreadMap.put(filename, new Log(tg, filename));
		}
		return logThreadMap.get(filename);
	}
	
	/**
	 * Shutdown all active log threads
	 */
	public static void shutDownLogs(){
		Run.log.log("Shutting down logs");
		tg.interrupt();
	}
	
	@Override
	public void finalize(){
		try {
			super.finalize();
			main.interrupt();
			logThreadMap.clear();
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Kills ALL background worker threads
	 */
	public static void killAll() {
		main.interrupt();
	}
	
}
