package com.trade_sim.framework;

import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;
/**
 * Used to managed long computations on the CPU. This allow additional concurrency in the program, and control over the process(job).
 * @author Samuel Palmer
 *
 */
public class Scheduler extends Thread{
	
	protected boolean run = true;
	protected Queue<Job> jobQueue = new LinkedBlockingQueue<Job>(1000);
	protected long counter;
//	protected DispatcherThread worker;
	
	public Scheduler(){
		start();
	}
	
	public boolean isRun() {
		return run;
	}

	/**
	 * Turns the scheduler on
	 * @param run
	 */
	public void setRun(boolean run) {
		this.run = run;
	}
	
	/**
	 * 
	 * @return the total number of jobs queued and executed
	 */
	public long getCounter() {
		return counter;
	}

	public Queue<Job> getJobQueue() {
		return jobQueue;
	}

	public Scheduler(ThreadGroup main, String string) {
		super(main, string);
		start();
	}

	/**
	 * Submit a new job to be queued to run.
	 * @param job
	 */
	public synchronized void submitJob(Job job){
		jobQueue.add(job);
		counter++;
		job.setId(counter);
	}
	
	/**
	 * Kills the currently running job. Currently this method does not work correctly. DO NOT USE.
	 * @param id
	 */
	public void kill(long id){
		Thread.yield();
		if(this.isInterrupted()){
			
		}
	}
	
	public void killAll(){
		
	}
	
	@Override
	public void run(){
		while(run){
			while (!jobQueue.isEmpty()){
				dispatch(jobQueue.poll());	
			}
		}
	}
	
	/**
	 * Dispatches the job to run on the cpu
	 * @param job the job to run on the cpu
	 */
	protected void dispatch(Job job){
		job.preRun();
		job.run();
		job.postRun();
	}
	
	
	
	

}
