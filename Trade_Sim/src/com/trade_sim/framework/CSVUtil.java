package com.trade_sim.framework;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.Collection;
import java.util.Vector;

import com.trade_sim.asset.Position;

/**
 * Provides general CSV file format writing and reading methods
 * @author Samuel Palmer
 *
 */
public class CSVUtil {
	
	/**
	 * Writes the collection to a new CSV file
	 * @param c collection of information to write
	 * @param file file path to write to
	 * @return if the write was successful
	 */
	public static boolean CollectionToCSV(Collection<CSVWriteable> c, String file){
		Config.createNewFolder(file);
		
		try {
			BufferedWriter bw = new BufferedWriter(new FileWriter(file));
			//Counter var
	
			for(CSVWriteable o : c){
				bw.write(o.toCSV());
				bw.newLine();
			}
			bw.flush();
			bw.close();
			return true;
			
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	/**
	 * Splits a CSV file into another file between the range of the specified lines
	 * @param start First line of the file is 0.
	 * @param end
	 * @param fileName File to be split
	 * @param newFileName File to be written to - will overwrite any existing file
	 * @return if the split was successful
	 */
	public static boolean splitCSV(int start, int end, String fileName, String newFileName){
		File file = new File(Config.temp);
		if(!file.exists()){
			file.mkdir();
		}
		
		try {
			BufferedReader br = new BufferedReader(new FileReader(fileName));
			BufferedWriter bw = new BufferedWriter(new FileWriter(newFileName));
			//Counter var
			int i = -1;
		Reading:while(br.ready()){
				i++; 
				if(i == start){
					while(br.ready() && i<=end){
					bw.write(br.readLine());
					bw.flush();
					bw.newLine();
					i++;
					}
					break Reading;
				}else{
				br.readLine();
				}
			}
			
			
			br.close();
			bw.flush();
			bw.close();
			return true;
			
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public static boolean PortfolioToCSV(Vector<Position> c, String file) {

		try {
			
			BufferedWriter bw = new BufferedWriter(new FileWriter(new File(file)));
			//Counter var
			bw.write(Position.CSVHeadings());
			bw.newLine();
			for(CSVWriteable o : c){
				bw.write(o.toCSV());
				bw.newLine();
			}
			bw.flush();
			bw.close();
			return true;
			
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

}
