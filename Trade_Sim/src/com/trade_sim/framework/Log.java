package com.trade_sim.framework;

import java.io.BufferedWriter;


import java.io.File;
import java.io.IOException;

import java.io.FileWriter;
import java.util.Calendar;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.Semaphore;

/**
 * The log class runs in its own thread and write strings to the specified file. The strings written
 * are controlled by a Queue and are written in the first come, first served basis.
 * @author Samuel Palmer
 *
 */
public class Log extends Thread{
private String file;
protected BlockingQueue<String> pq = new LinkedBlockingQueue<String>(1000000);
private boolean Writing; //semaphore could be used but by using this simple flag it allows control of the write method
protected boolean localRun = false;
	@Deprecated
	public Log(){
		super();
		file = "log.txt";
		
		start(); 
	}
	
	public Log(ThreadGroup tg, String fileName){
		super(tg,fileName);
		file = fileName;
		try {
			new File(file).createNewFile();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		pq.clear();
		start(); 
	}
	
	
	public Log(String fileName){
		file = fileName;
		try {
			new File(file).createNewFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		start(); 
	}
	
	@Override
	public void run(){
		//System.out.println("Log Thread booted " +file);
		
		while(Run.running ){
			if (!Writing){
				if(Run.logging || this.localRun)
				writeToLog();
			}
			try{
				Thread.sleep(500);
			}catch(InterruptedException e){
				writeToLog();
				Thread.currentThread().interrupt();
				break;
			}
			
			
		}
		//System.out.println("Log Thread shutting down " +file);
	}
	
	/**
	 * Adds a string to the queue to be written to the log file
	 * @param s string to write
	 */
	public synchronized void log(String s){
		if(Run.logging || this.localRun ){
			try {
				pq.put(s);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	private void writeToLog(){
	//	System.out.println("Writing");
		try{
			Writing = true;
			
			BufferedWriter bw = new BufferedWriter(new FileWriter(file, true));
		while(!pq.isEmpty()){

				bw.write(Calendar.getInstance().getTime().toString() +" " +pq.remove());
				bw.newLine();
				
		}
		
		bw.flush();
		bw.close();
		Writing = false;
		}catch(Exception e){Writing = false; e.printStackTrace();}
	}
	
	
	public void setWrite(boolean write){
		this.localRun = write;
	}
	
	

}
