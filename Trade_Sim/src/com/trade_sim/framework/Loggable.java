package com.trade_sim.framework;

/**
 * Class used to provide logging functionality
 * @author Samuel Palmer
 *
 */
public abstract class Loggable {
	protected Log log;
	protected String defaultPath = this.getClass().getCanonicalName() +".txt";
	
	/**
	 * Uses the default file path, which is the name of the canonical name of the class
	 */
	public Loggable(){
		this.log = ThreadPool.getLogThread(defaultPath);
	}
	
	/**
	 * 
	 * @param filePath
	 */
	public Loggable(String filePath){
		this.log = ThreadPool.getLogThread(filePath);
	}
	
	/**
	 * Changes the write file path. Default file path is the name of the canonical name of the class
	 * @param newFilePath
	 */
	public void changePath(String newFilePath){
		this.log.interrupt();
		this.log = ThreadPool.getLogThread(newFilePath);
	}
	
	/**
	 * Sets the log to write
	 * @param write
	 */
	public void setLogWrite(boolean write){
		this.log.setWrite(write);
	}
	
	/**
	 * Writes the log the current state of the object
	 */
	public void logState(){
		if(this.getLoggableState() !=null)
		this.log.log(this.getLoggableState());
	}
	
	/**
	 * Gets the state of the object in string form
	 * @return the loggable state of the object
	 */
	protected abstract String getLoggableState();
	
	@Override
	public void finalize(){
		this.log.interrupt();
		try {
			super.finalize();
		} catch (Throwable e) {
			e.printStackTrace();
		}
		
	}

}
