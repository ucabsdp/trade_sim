package com.trade_sim.framework;

import java.io.File;

public class ParentFolder {
	
	protected String folderPath;

	public ParentFolder(String folderPath){
		this.folderPath = folderPath;
		Config.createNewFolder(folderPath);
	}

}
