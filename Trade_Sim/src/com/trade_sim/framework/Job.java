package com.trade_sim.framework;

/**
 * A job can be scheduled and ran using the {@link Scheduler} this allows long computations to be queued for execution.
 * @author Samuel Palmer
 *
 */
public interface Job {

	void preRun();

	void run();

	void postRun();

	void setId(long ID);
	
    boolean isFinished();
	
    double getProgress();
	

}
