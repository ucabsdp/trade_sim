package com.trade_sim.framework;

import java.io.File;
/**
 * Contains all of the general system settings, such as folder paths. As well as providing 
 * default utility methods
 * @author Samuel Palmer
 *
 */
public class Config {
	public static String LOGFOLDER = "Logs";
	public static String OptimizationFolder = "Optimizations";
	public static String temp = "temp";
	public static String SIMULATIONFOLDER = "Simulations";
	
	
	/**
	 * Creates a new folder or series of nested folders given the path
	 * @param path
	 */
	public static void createNewFolder(String path){
		String[]  folders = path.split(File.separator);
		StringBuilder path_ = new StringBuilder();
		int counter = 0;
		for(String folder : folders){
			path_.append(folder);
		File file = new File(path_.toString());
		if(!file.exists()){
			file.mkdir();
		}
			counter++;
			if(counter != folders.length){
				path_.append(File.separator);
		}
		}
	}

}
