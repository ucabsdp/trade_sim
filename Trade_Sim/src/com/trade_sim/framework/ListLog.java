package com.trade_sim.framework;

import java.util.Collection;

/**
 * A log implementation {@link log}, that writes collections of objects in string form to
 * the log file
 * @author Samuel Palmer
 *
 */
public class ListLog extends Log {

	public ListLog(String fileName) {
		super(fileName);
		// TODO Auto-generated constructor stub
	}

	public ListLog(ThreadGroup tg, String fileName) {
		super(tg, fileName);
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * Collection of objects to be written
	 * @param c
	 */
	public synchronized void log(Collection c){
		
		if(Run.logging || this.localRun ){
			try {
				for(Object o : c){
				pq.put(o.toString());
				}
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}
