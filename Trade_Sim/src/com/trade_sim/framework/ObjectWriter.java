package com.trade_sim.framework;

import java.io.FileInputStream;

import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;



/**
 * Provides static object writing and reading methods for serializable objects.
 * @author Samuel Palmer
 *
 */
public class ObjectWriter {
	
	/**
	 * Writes a new object to a given file name
	 * @param o object to be written
	 * @param filename file path of target file
	 * @return if the write was successful 
	 */
	public static boolean writeObject(Object o, String filename){
		ObjectOutputStream out;
		File f = new File(filename);
		try {
			out = new ObjectOutputStream(new FileOutputStream(filename));
			out.writeObject(o);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	/**
	 * Loads the object from the given file path
	 * @param filename file path to read from
	 * @return the object that has be loaded, otherwise null if the load
	 * was unsuccessful 
	 */
	public static Object loadObject(String string) {
		ObjectInputStream in;
		Object o = null;
		try {
			in =  new ObjectInputStream(new FileInputStream(string));
			o = in.readObject();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return o;
	}

}
