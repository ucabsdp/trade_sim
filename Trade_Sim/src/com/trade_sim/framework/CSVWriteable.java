package com.trade_sim.framework;

/**
 * Allows the object to be written to files in CSV format {@link CSVUtil} 
 * @author Samuel Palmer
 *
 */
public interface CSVWriteable {
	
	/**
	 * Method called to get the object state as a CSV string.
	 * @return
	 */
	public String toCSV();
	

}
