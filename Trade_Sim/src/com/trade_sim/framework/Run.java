package com.trade_sim.framework;

import com.trade_sim.broker.Brokerage;
import com.trade_sim.platform.Platform;
import com.trade_sim.platform.PlatformFactory;
import com.trade_sim.strategy.Strategy;
import com.trade_sim.strategy.TestStategy;

/**
 * Main class used to start-up and run the program. This creates the first workspace {@link SavableWorkspace} instance,
 * Main controller {@link MainController} instance; and kills all threads once the program has finished running
 * @author Samuel Palmer
 *
 */
public class Run {
	public static Log log = ThreadPool.getLogThread("MainLog.txt");
	public static volatile boolean running = true;
	public static boolean debug = true;
	public static boolean logging = true;
	//private static Platform platform = null;
	
	/**
	 * Example simulation
	 * @param args
	 */
	public static void main(String[] args) {
		startup();
		
		/*
		 * Create a new trading platform using a CSV file (check is meets the CSV specification
		 * still need to add custom CSV format handling) default is yahoo finance csv format.
		 * 
		 * Then set up a broker with transaction fees, either percentage of trade value or fixed.
		 */
		Platform p = PlatformFactory.getDefault("dailyData1.csv");
		p.getBroker().setFee(Brokerage.FeeType.PERCENTAGE, 1.00);
		
		Strategy test = new TestStategy(p);
		test.setAsset("dailyData1.csv");
		
		p.addStrategy(test);
		//Will track the performance of trading over the simulation
		p.getPortfolioManager().setLiveUpdate(true);
		p.startSimulation();
		//print the final portfolio of trades to the log
		p.getPortfolioManager().logDefaultPortfolio();
		double r = p.getPortfolioManager().getDefaultPortfolio().calculateCurrentReturn();
		double pl = p.getPortfolioManager().getDefaultPortfolio().getCurrentPL();
		double c = p.getPortfolioManager().getDefaultPortfolio().getCost();
		p.getPortfolioManager().getDefaultPortfolio().writeToCSV("default.csv");
		System.out.println("Return = " +r);
		System.out.println("PL = " +pl);
		System.out.println("Cost = " +c);
		shutdown();
	}
	
	public static void startup(){
		log.log("--------------Starting New Boot ----------------");
		log.setWrite(true);
	}
	
	public static void shutdown(){
		ThreadPool.shutDownLogs();
	}

	

}

