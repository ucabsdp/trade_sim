package com.trade_sim.platform;

import java.util.ArrayList;

import java.util.Date;

import com.trade_sim.asset.Asset;
import com.trade_sim.asset.AssetListener;
import com.trade_sim.asset.Position;
import com.trade_sim.bank.Bank;
import com.trade_sim.broker.Broker;
import com.trade_sim.broker.Brokerage;
import com.trade_sim.framework.GLOBAL;
import com.trade_sim.framework.Run;
import com.trade_sim.market.MarketAccess;
import com.trade_sim.market.SequentialMarketAccess;
import com.trade_sim.portfolio_mangement.PortfolioManager;
import com.trade_sim.strategy.Strategy;



public class BasicPlatform extends Platform{
protected String version = "0.1";
protected PlatformSettings settings;
protected int delay = -1;
protected boolean isDelay;
protected boolean wasDelay;
protected boolean isLiveHistory;
protected ArrayList<Strategy> strategies = new ArrayList<Strategy>();
protected ArrayList<Position> openPositions = new ArrayList<Position>();
protected MarketAccess access;
protected PortfolioManager pManager= new PortfolioManager(this);
protected Bank bank;
private Date dateDelay;
protected boolean pause;
private boolean isTradingActive = true;
private boolean isSimulating;

	/**
	 * Recommended to use {@link PlatformFactory}
	 */
	public BasicPlatform(){
		Run.log.log("Platform init using version " +getVersion());
	}
	
	/**
	 * Recommended to use {@link PlatformFactory}
	 * @param fileName
	 * @throws Exception
	 */
	public BasicPlatform(String fileName) throws Exception{
			this(new SequentialMarketAccess(fileName));
	}
	
	/**
	 * Recommended to use {@link PlatformFactory}
	 * @param access
	 */
	public BasicPlatform(MarketAccess access){
			this(access, null,null);
		
	}
	
	/**
	 * Recommended to use {@link PlatformFactory}
	 * @param access
	 * @param brokerage
	 * @param bank
	 */
	public BasicPlatform(MarketAccess access, Brokerage brokerage, Bank bank){
		this.access = access;
		
		if(bank == null){
			this.bank = bank;
		}else{
			this.bank = bank;
		}
		
		if(brokerage == null){
		this.broker = new Broker(this.access);
		}else{
		this.broker = brokerage;
		}
		
		this.access.registeredAssets = this.registeredAssets;
		Run.log.log("Platform init using version " +getVersion());
	}
	
	@Override
	public void addMarket(MarketAccess marketAccess) {
		// TODO Auto-generated method stub
	}

	/**
	 * Returns the current platform version being used
	 */
	@Override
	public String getVersion() {
		return this.version;
	}
		
	/**
	 * Flushes the system with delayed data {@link setSimulationDelay}
	 * By default this method is called at the started of a simulation. It resets the time after the flush
	 * and all flushed data is does not appear in any history.
	 * During a flush by default, Asset history is turned off and TRADING IS SET FALSE.
	 * After asset history is set back to its original setting and trading is set true.
	 */
	@Override
	public void flush(int i){
		try {

			if(!access.isStreamActive())
			access.connect();
			
			this.setTradingActive(false);
			Asset.isLiveHistory = false;
			
			int counter = 0;
			Run.log.log("Flush started " +i);
			while(access.isStreamActive() && counter <= i){
				access.getMarket().getNewPrice();
				counter++;
			}
		} catch (Exception e) {
			Run.log.log("Error flushing simulation");
			e.printStackTrace();
		}
			GLOBAL.resetTime();
			Asset.isLiveHistory = this.isLiveHistory;
			this.setTradingActive(true);
	}
	
	
	/**
	 * Start the simulation running
	 */
	@Override
	public void startSimulation() {
		this.settings = this.exportToSettings();
		this.isSimulating = true;
		if(isDelay){
			flush(this.delay);
		}
		
		try {
			if(!access.isStreamActive())
			access.connect();
			
			Run.log.log("Simulation started " +this.hashCode());
			
			while(access.isStreamActive()){	
				while(pause){
				}
	 		access.getMarket().getNewPrice();
			}
			
		} catch (Exception e) {
			Run.log.log("Error staring simulation");
			e.printStackTrace();
		}
		endSimulation();
	}
	
	
	/**
	 * Sets the number of simulation timesteps to delay for. This delay allows the system to 
	 * pre-calculate any instruments, advised to use for MA. During a delay no trades will be placed by strategies, but data 
	 * will be active. At the end of the delay any active strategies will place trades, active history will be recorded and 
	 * the timeStep will be reset to 0;
	 * @param delay Timesteps to wait
	 */
	@Override
	public void setSimluationDelay(final int delay){
		this.isDelay = true;
		this.wasDelay = true;
		this.delay = delay;
	}
	
	/**
	 * Sets the number of simulation timesteps to delay for. This delay allows the system to 
	 * pre-calculate any instruments, advised to use for MA. During a delay no trades will be placed by strategies, but data 
	 * will be active. At the end of the delay any active strategies will place trades, active history will be recorded and 
	 * the timeStep will be reset to 0;
	 * @param delay Timesteps to wait
	 */
	@Override
	public void setSimluationDelay(final Date delay){
	}
	
	public boolean isDelay(){
		return this.isDelay;
	}

	@Override
	public void endSimulation() {
		access.disconnect();
		this.isSimulating = false;
		Run.log.log("SimulationFrame stopped");
	}

	@Override
	public void resetSimulation() {
		this.isDelay = this.wasDelay;
		this.clearStrategies();
		this.pManager.clear();
	}

	@Override
	public boolean isSimulating() {
		return this.isSimulating;
	}
	
	/**Registers an assetID and creates a new Asset object to detect price changes in the asset price. 
	 * 
	 * @param ID The ID of the asset to be registered
	 */
	protected void registerAsset(String ID){
		access.registerAsset(ID);
	}
	
	@Override
	public void registerAsset(AssetListener a){
		access.registerAsset(a);
	}
	
	protected void registerAssetlp(AssetListener a){
		access.registerAssetLp(a);
	}
	
	@Override
	public Asset getAsset(String s){
		if(registeredAssets.containsKey(s)){
			return registeredAssets.get(s);
		}
		else{
			return null;
		}
	}

	
	//TRADE CONTROL METHODS

	
	@Override
	public 	Position openPosition(Trade trade) {
		Position p = null;
		if(this.isTradingActive){
			p =  broker.placeTrade(trade);
		if(p != null){
			pManager.addToPortfolio(p);
		}
		}
		return p;
	}

	@Override
	public void cancelTrade(Trade trade) {
		
	}

	@Override
	public void closePosition(Trade trade) {
	}
	
	
	@Override
	public PortfolioManager getPortfolioManager(){
		return this.pManager;
	}
	
	//STRATEGY CONTROL METHODS

	@Override
	public void addStrategy(Strategy strategy) {
		strategy.setPlatform(this);
		strategies.add(strategy);
		if(strategy.getAssetID() !=null){
			registerAsset(strategy.getAssetID());
			registeredAssets.get(strategy.getAssetID()).addListener((AssetListener)strategy);
		}
		Run.log.log("Strategy added " +strategy.toString());
		
	}

	@Override
	public void removeStrategy(Strategy strategy) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ArrayList<Strategy> getStrategyList() {
		return this.strategies;
		
	}
	
	/**
	 * Turn on a strategy to start trading
	 * @param i Strategy index
	 */
	public void activateStrategy(int i){
		this.strategies.get(i).setTradingActive(true);
	}
	
	/**
	 * Turn off a strategy trading
	 * @param i Strategy index
	 */
	public void deavtivateStrategy(int i){
		this.strategies.get(i).setTradingActive(false);
	}
	

	@Override
	public void clearStrategies() {
		this.strategies = new ArrayList<Strategy>();
		
	}
	
	@Override
	public String toString(){
		return "Platform "+this.version+
				" Using : MarketAccess "+this.access.toString()
				+", Bank "+this.bank.toString();
	}

	@Override
	public void setAssetHistoryCapture(boolean capture) {
		this.isLiveHistory = capture;
		Asset.isLiveHistory = capture;
	}

	@Override
	public void setTradingActive(boolean active) {
		this.isTradingActive = active;
	}
	
	/**
	 * Configures this platform using {@link PlatformSettings} and stores the configuration for reset.
	 * @param settings
	 */
	public void configure(PlatformSettings settings){
		this.setAssetHistoryCapture(settings.assetHistory);
		if(settings.isDateDelay){
		this.setSimluationDelay(settings.dateDelay);
		}
		if(settings.isTimeStepDelay){
		this.setSimluationDelay(settings.timeStepDelay);
		}
		if(settings.portfolioIsLive){
		this.getPortfolioManager().setLiveUpdate(settings.portfolioIsLive);
		}
		this.settings = settings;
	}
	
	public PlatformSettings exportToSettings(){
		PlatformSettings s = new PlatformSettings();
		s.isDateDelay = this.isDelay;
		s.timeStepDelay = this.delay;
		s.portfolioIsLive = this.getPortfolioManager().isLiveUpdate();
		s.assetHistory = this.isLiveHistory;
		return s;
	}
	
	/**
	 * Pauses the current simulation by holding it in a spin lock
	 * @param pause
	 */
	@Override
	public synchronized void pause(boolean pause){
		this.pause = pause;
	}
	


}
