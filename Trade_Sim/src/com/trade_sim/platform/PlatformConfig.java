package com.trade_sim.platform;

import java.util.Date;

/**
 * All the configurable methods for the platform
 * @author Samuel Palmer
 *
 */
public interface PlatformConfig {
	/**
	 * See {@link BasicPlatform}
	 * @param delay
	 */
	public void setSimluationDelay(int delay);
	/**
	 * See {@link BasicPlatform}
	 * @param delay
	 */
	public void setSimluationDelay(Date delay);
	/**
	 * Set the assets to capture all past data and hold in the history. If charting is not required
	 * set this to false as it lowers memory overheads and enchances performance. It must be active if charting
	 * is required otherwise there will be no price data to display
	 * @param capture
	 */
	public void setAssetHistoryCapture(boolean capture);

}
