package com.trade_sim.platform;

import com.trade_sim.asset.AssetListener;
import com.trade_sim.asset.AssetPrice;


/**
 * A general asset listener that keeps track of price changes for the given asset. Designed for primary use as a display element
 * @author Samuel Palmer
 *
 */
public class Ticker implements AssetListener{
	protected String ID;
	protected AssetPrice price;
	
	public Ticker(String ID){
		this.ID = ID;
	}

	@Override
	public void update(AssetPrice price) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getAssetId() {
		return ID;
	}

}
