package com.trade_sim.platform;

import com.trade_sim.broker.Brokerage;
import com.trade_sim.market.Market;
import com.trade_sim.market.MarketAccess;

/**
 * Builds and instantiates the latest platform setup using the desired parameters
 * @author Samuel Palmer
 *
 */
public class PlatformFactory {
	
	/**
	 * 
	 * @return Returns the latest platform version
	 */
	public static Platform getDefault(){
		return new BasicPlatform();
	}
	
	/**
	 * Returns the latest platform version, instantiated with default settings and passed
	 * market access
	 * @param ma latest platform uses the specified MarketAccess and all other parameters set to default
	 * @return
	 */
	public static Platform getDefault(MarketAccess ma){
		return new BasicPlatform(ma);
	}
	
	/**
	 * 
	 * @param csv
	 * @return Latest platform with default settings and csv market
	 */
	public static Platform getDefault(String csv){
		try {
			return new BasicPlatform(csv);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 *  Returns the latest platform version, instantiated with the Market and Broker
	 * @param market Market to use
	 * @param brokerage Broker to use
	 * @return
	 */
	public static Platform getDefault(Market market, Brokerage brokerage){
		try{
			brokerage.setMarketAccess(market.getMarketAccess());
			return new BasicPlatform(market.getMarketAccess(),brokerage,null);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 
	 * @param settings Platform settings {@link PlatformSettings}
	 * @return The latest platform version instantiated with settings
	 */
	public static Platform getDefault(PlatformSettings settings){
		Platform p  = new BasicPlatform();
		p.setAssetHistoryCapture(settings.assetHistory);
		if(settings.isDateDelay){
		p.setSimluationDelay(settings.dateDelay);
		}
		if(settings.isTimeStepDelay){
		p.setSimluationDelay(settings.timeStepDelay);
		}
		return p;
	}
	
	/**
	 * Configures a pre-existing platform using {@link PlatformSettings}
	 * @param p
	 * @param settings
	 */
	public static void configure(Platform p, PlatformSettings settings){
		p.setAssetHistoryCapture(settings.assetHistory);
		if(settings.isDateDelay){
		p.setSimluationDelay(settings.dateDelay);
		}
		if(settings.isTimeStepDelay){
		p.setSimluationDelay(settings.timeStepDelay);
		}
		if(settings.portfolioIsLive){
		p.getPortfolioManager().setLiveUpdate(settings.portfolioIsLive);
		}
	}
	


}
