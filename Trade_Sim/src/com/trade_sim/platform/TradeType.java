package com.trade_sim.platform;

/**
 * The different types of trades allowed to be made.
 * @author Samuel Palmer
 *
 */
public enum TradeType {
BUY(1), SELL(-1), LIMIT(0), STOP(0), ORDER(0);

private int i;
TradeType(int i){
	this.i=i;
}

public int getValue(){
	return i;
}

}