package com.trade_sim.platform;

import java.util.ArrayList;

import java.util.Date;
import java.util.HashMap;

import com.trade_sim.asset.Asset;
import com.trade_sim.asset.AssetListener;
import com.trade_sim.asset.Position;
import com.trade_sim.broker.Brokerage;
import com.trade_sim.market.MarketAccess;
import com.trade_sim.portfolio_mangement.PortfolioManager;
import com.trade_sim.strategy.Strategy;


public abstract class Platform implements PlatformConfig{
	protected Brokerage broker;

	
	final HashMap<String, Asset> registeredAssets = new HashMap<String, Asset>(20);
	/**
	 * Places a request for a new trade position with the platforms broker
	 * @param trade
	 * @return
	 */
	public abstract Position openPosition(Trade trade);
	public abstract void cancelTrade(Trade trade);
	public abstract void closePosition(Trade trade);
	
	/**
	 * Starts the trading simulation; activates the data stream
	 */
	public abstract void startSimulation();
	public abstract void endSimulation();
	/**
	 * Reset the simulation back to its last known startinn configuration
	 */
	public abstract void resetSimulation();
	/**
	 * 
	 * @return if a simulation is running
	 */
	public abstract boolean isSimulating();
	
	/**
	 * Register a new strategy with the platform to trade with
	 * @param strategy
	 */
	public abstract void addStrategy(Strategy strategy);
	/**
	 * Removes a strategy from the platform
	 * @param strategy
	 */
	public abstract void removeStrategy(Strategy strategy);
	/**
	 * Gets all the currently registered strategies using this platform
	 * @return
	 */
	public abstract ArrayList<Strategy> getStrategyList();
	/**
	 * Remove all the strategies
	 */
	public abstract void clearStrategies();
	
	public abstract Asset getAsset(String s);
	
	public abstract void addMarket(MarketAccess marketAccess);
	/**
	 * 
	 * @return the current version of the platform being used
	 */
	public abstract String getVersion();
	public abstract  PortfolioManager getPortfolioManager();
	/**
	 * Register a new AssetListener to the current platform
	 * @param a
	 */
	public abstract void registerAsset(AssetListener a);
	/**
	 * Allows trade to be made. Otherwise all trades will not be sent to the broker
	 * @param active
	 */
	public abstract void setTradingActive(boolean active);
	public void flush(int i) {
		// TODO Auto-generated method stub
		
	}
	/**
	 * Temporarily suspends the simulation. IsSimulating variable will still be true.
	 * @param pause
	 */
	public abstract void pause(boolean pause);
	
	public Brokerage getBroker() {
		return broker;
	}
	
	public void setBroker(Brokerage broker) {
		this.broker = broker;
	}
	

	
}
