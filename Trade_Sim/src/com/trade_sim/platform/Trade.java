package com.trade_sim.platform;

/**
 * Holds the information for a requested position to be opened.
 * @author Samuel Palmer
 *
 */
public class Trade {
	
	private String ID;
	private int quantity;
	private TradeType type;
	private TradeType direction;
	private double price;
	private double priceBounds;
	private Trade stop;
	private Trade limit;
	
	
	public Trade(){
		
	}
	
	
	public String getID() {
		return ID;
	}
	
	/**
	 * Set the asset ID to be traded
	 * @param iD
	 */
	public void setID(String iD) {
		ID = iD;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public TradeType getType() {
		return type;
	}
	/**
	 * Set the type of trade, whether it is a stop, limit trade or order.
	 * @param type
	 */
	public void setType(TradeType type) {
		this.type = type;
	}
	public TradeType getDirection() {
		return direction;
	}
	/**
	 * Set the direction, (long or short) of the trade
	 * @param direction either BUY(long) or Sell(short)
	 */
	public void setDirection(TradeType direction) {
		this.direction = direction;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public double getPriceBounds() {
		return priceBounds;
	}
	/**
	 * The price bounds is the given deviation the current market price is allowed to vary from the requested trade price.
	 * If the current price is outside the bounds the trade will not be made.
	 * @param priceBounds
	 */
	public void setPriceBounds(double priceBounds) {
		this.priceBounds = priceBounds;
	}
	public Trade getStop() {
		return stop;
	}
	/**
	 * Set a corresponding stop loss trade to exit at a specific price level for unfavorable movements
	 * @param stop
	 */
	public void setStop(Trade stop) {
		this.stop = stop;
	}
	
	public Trade getLimit() {
		return limit;
	}
	/**
	 * Set a corresponding limit trade to exit at a specific profiting price level 
	 * @param limit
	 */
	public void setLimit(Trade limit) {
		this.limit = limit;
	}
	
	@Override
	public String toString(){
		return ID +" " 
				+quantity +" "
				+type.toString() +" "
				+direction.toString() +" Price:"
				+price +" Price Bounds:"
				+priceBounds;
				
	}

}
