package com.trade_sim.platform;



import java.util.Date;

import com.trade_sim.framework.Setting;
import com.trade_sim.market.MarketType;


/**
 * Bean class containing all the configuration settings for a platform. To then configure an empty platform
 * use {@link PlatformFactory} configure() method, or the prefered {@link BasicPlatform}.configure() method
 * @author Sameul Palmer
 *
 */
public class PlatformSettings extends Setting implements PlatformConfig {


	public boolean isDateDelay;
	public boolean isTimeStepDelay;
	public int timeStepDelay;
	public Date dateDelay;
	public boolean assetHistory;
	public MarketType marketType;
	public boolean portfolioIsLive;
	
	@Override
	public void setSimluationDelay(int delay) {
		isTimeStepDelay = true;
		isDateDelay = false;
		timeStepDelay = delay;
		
	}
	@Override
	public void setSimluationDelay(Date delay) {
		isTimeStepDelay = false;
		isDateDelay = true;
		dateDelay = delay;
		
	}
	
	@Override
	public void setAssetHistoryCapture(boolean capture) {
		assetHistory = capture;
	}
	

}
