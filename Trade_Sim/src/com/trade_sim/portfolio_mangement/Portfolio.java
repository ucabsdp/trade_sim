package com.trade_sim.portfolio_mangement;
import java.util.Collection;

import java.util.HashMap;
import java.util.Vector;

import com.trade_sim.asset.AssetPrice;
import com.trade_sim.asset.Position;
import com.trade_sim.framework.CSVUtil;
import com.trade_sim.framework.CSVWriteable;

/**
 * Holds all the open and historic positions 
 * @author Samuel Palmer
 *
 */
public class Portfolio {
	protected HashMap<String,Vector<Position> > positions = new HashMap<String, Vector<Position>>(10);
	protected Vector<String> assetIDs = new Vector<String>();
	protected Vector<Position> positionList = new Vector<Position>();
	protected Vector<Position> openPositions = new Vector<Position>();
	protected Vector<Position> closedPositions = new Vector<Position>();
	
	
	public Portfolio(){}
	public Portfolio(int size){
		positions = new HashMap<String, Vector<Position>>((int)(size*1.6));
	}
	
	/**
	 * Adds a new position to the portfolio and closes and corresponding opened portfolios
	 * @param p position to be added
	 */
	public void addPosition(Position p){
		positionList.add(p);
		if(!assetIDs.contains(p.getAssetId())){
			assetIDs.add(p.getAssetId());
		}
	}
	

	/**
	 * Calculates the sum of the profit for the portfolio
	 * @return P&L for the total portfolio at current prices
	 */
	public synchronized double updatePositions(AssetPrice a){
		double r = 0;
		for(Position p : positionList){
			if(p.getAssetId() == a.ID){
//			this.log.log(p.toString());
//			System.out.println(p.toString());
			r += p.update(a);
			}
		}
			
		return r;
	}
	
	/**
	 * Calculates the sum of log returns 
	 * @return Total log return for the total portfolio at current prices
	 */
	public synchronized double calculateReturn(AssetPrice a){
		double r = 0;
		double cost = 0;
		for(Position p : positionList){
			if(p.getAssetId() == a.ID){
//			this.log.log(p.toString());
//			System.out.println(p.toString());
			r += p.calculateProfit(a.close);
			cost += p.cost();
			}
		}

		return (r/cost)*100;
	}
	
	/**
	 * Calculates the sum of log returns 
	 * @return Total log return for the total portfolio at current prices
	 */
	public double calculateCurrentReturn(){
		double r = 0;
		double cost = 0;
		for(Position p : positionList){
//			this.log.log(p.toString());
//			System.out.println(p.toString());
			r += p.getProfit();
			cost += p.cost();
			}
		return (r/cost)*100;
	}
	
	public double getCost(){
		double cost = 0;
		for(Position p : positionList){
			cost += p.cost();
			}
		return cost;
	}
	
	public double getCurrentPL(){
		double r = 0;
		for(Position p : positionList){
			r += p.getProfit();
			}
		return r;
	}
	
	/**
	 * Get the complete position history of this portfolio. This includes open positions, closed positions 
	 * and closing positions(those used to close open positions)
	 * @return
	 */
	public Collection<Position> getPortfolio(){
		return this.positionList;
	}
	
	/**
	 * Get all the closed positions
	 * @return
	 */
	public Collection<Position> getClosePositions(){
		return closedPositions;
		
	}
	
	/**
	 * Get all the currently open positions
	 * @return
	 */
	public Collection<Position> getOpenPositions(){
		return (Collection<Position>)openPositions;
		
	}
	
	/**
	 * If this portfolio contains positions for the asset
	 * @param id Asset ID
	 * @return
	 */
	public boolean containsAssetPosition(String id){
		return this.assetIDs.contains(id);
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Portfolio [positions=" + positionList.toString() + "]";
	}
	/**
	 * 
	 * @return If this portfolio is empty or not
	 */
	public boolean isEmpty() {
		return (this.positionList.isEmpty() && this.positions.isEmpty())? true:false;
	}
	
	public void writeToCSV(String file){
		CSVUtil.PortfolioToCSV(this.positionList, file);
	}
}
