package com.trade_sim.portfolio_mangement;

import java.util.HashMap;


import java.util.Map.Entry;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.Vector;

import com.trade_sim.asset.AssetListener;
import com.trade_sim.asset.AssetPrice;
import com.trade_sim.asset.Position;
import com.trade_sim.framework.Loggable;
import com.trade_sim.platform.Platform;

/**
 * The main component that manages a simulations portfolios. New portfolios can be created and existing positions added to them.
 * The portfolio manager is also responsible for keeping all the open positions up to date with the
 * latest price information. This method is ran inside an internal thread that holds a queue of
 * new price information. The portfolio manager can listen to multiple assets.
 * All positions are held, and can be accessed from the default portfolio. Whilst each asset also
 * has its own portfolio containing all the positions taken in that asset.
 * 
 * Future devlopment will lower memory loads of this class by using an external database to hold
 * past and temporary positions.
 * @author Samuel Palmer
 *
 */
public class PortfolioManager extends Loggable implements AssetListener{
	private HashMap<String, Portfolio> portfolios = new HashMap<String, Portfolio>(5);
	private Portfolio defaultPortfolio = new Portfolio();
	private Vector<String> registeredAssets = new Vector<String>();
	protected Platform p;
	protected double currentValue;
	
	//Updating varibles
	protected boolean liveUpdate = false;
	protected BlockingQueue<AssetPrice> pq;
	protected volatile boolean updating;
	
//-----------------------------------------------
	/**
	 * 
	 * @param p platform for the manager to use
	 */
	public PortfolioManager(Platform p){
		super("portfolioManager.txt");
		this.p = p;
	}
	
	/**
	 * Returns all the portfolios held
	 * @return
	 */
	public HashMap<String, Portfolio> getPortfolios() {
		return portfolios;
	}

	/**
	 * Gets the default portfolio that holds a reference to all the positions taken
	 * @return
	 */
	public Portfolio getDefaultPortfolio() {
		return defaultPortfolio;
	}
	
	/**
	 * Writes the default portfolio to the log file
	 */
	public void logDefaultPortfolio(){
		this.log.log(this.defaultPortfolio.toString());
	}

	/**
	 * Adds position to the default portfolio
	 * @param p Position to be added
	 */
	public void addToPortfolio(Position p){
		defaultPortfolio.addPosition(p);
		
		if(liveUpdate && !this.registeredAssets.contains(p.getAssetId())){
			this.addAssetListener(p.getAssetId());
		}
	}
	
	/**
	 * Adds a new position to given the portfolio
	 * @param portfolio
	 * @param p
	 */
	public void addToPortfolio(String portfolio, Position p){
		if(portfolios.containsKey(portfolio)){
			portfolios.get(portfolio).addPosition(p);
		}else{
		defaultPortfolio.addPosition(p);
		}
		// If the system is live updating register this asset to be listened to if it is already not
		if(liveUpdate && !this.registeredAssets.contains(p.getAssetId())){
			this.addAssetListener(p.getAssetId());
		}
	}
	
	/**
	 * Create a new portfolio under the given name
	 * @param portfolio
	 */
	public void newPortfolio(String portfolio){
		if(!portfolios.containsKey(portfolio)){
			portfolios.put(portfolio, new Portfolio());
		}
	}
	
	/**
	 * Remove the portfolio of the given name
	 * @param portfolio
	 */
	public void deletePortfolio(String portfolio){
		if(portfolios.containsKey(portfolio)){
			if(portfolios.get(portfolio).isEmpty()){
				portfolios.remove(portfolio);
			}
		}
		
	}
	
	/**
	 * Clears all held portfolios
	 */
	public void clear(){
		portfolios = new HashMap<String, Portfolio>(5);
		defaultPortfolio = new Portfolio();
	}

	@Override
	public void update(AssetPrice price) {
		if(liveUpdate){
		pq.add(price);
		updatePortfolios();
		}
	}
	
	/**
	 * Updates all the positions with the latest price information. This is ran in a seperate thread
	 * and is done in the background
	 */
	private void updatePortfolios(){
		
		System.out.println("Updating live portfolios " +this.getAssetId());
		if(!updating){
			updating = true;
			
		new Runnable(){
			private AssetPrice price;
			@Override
			public void run() {
				while(!pq.isEmpty()){
				double totalPl = 0;
				 price = pq.poll();
				//Check the default portfolio
				if(defaultPortfolio.containsAssetPosition(price.ID)){
					totalPl += defaultPortfolio.updatePositions(price);
				}
				//Check the registered portfolio
				if(!portfolios.isEmpty()){
					Portfolio p = null;
					for(Entry e : portfolios.entrySet())
					  p = (Portfolio) e.getValue();
					  totalPl += p.updatePositions(price);
					  System.out.println(p.toString());
					}
				System.out.println("Current pl : " +totalPl);
				currentValue = totalPl;
				log.log("Current Profit/loss = " +totalPl);
				
				}
				updating = false;
				
			}
			
		}.run();
		}
		
	}
	
	/**
	 * Adds the portfolioManager to listen to live price changes
	 * @param s
	 */
	private void addAssetListener(String s){
		if(!registeredAssets.contains(s)){
		this.registeredAssets.add(s);
		p.registerAsset(this);
		}
	}

	@Override
	public String getAssetId() {
		return this.registeredAssets.lastElement();
	}

	/**
	 * Checks if live position price updating is being used
	 * @return
	 */
	public boolean isLiveUpdate() {
		return liveUpdate;
	}

	/**
	 * Turns on live position price updating
	 * @param liveUpdate
	 */
	public void setLiveUpdate(boolean liveUpdate) {
		this.liveUpdate = liveUpdate;
		if(this.liveUpdate){
			pq = new LinkedBlockingQueue<AssetPrice>(1000);
		}
	}

	@Override
	protected String getLoggableState() {
		// TODO Auto-generated method stub
		return null;
	}

	public double getCurrentValue() {
		return currentValue;
	}

}
