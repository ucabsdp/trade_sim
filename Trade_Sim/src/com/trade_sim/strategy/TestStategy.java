package com.trade_sim.strategy;

import com.trade_sim.framework.Run;
import com.trade_sim.platform.Platform;
import com.trade_sim.platform.Trade;
import com.trade_sim.platform.TradeType;

public class TestStategy extends AbstractStrategy {

	public TestStategy(Platform platform) {
		super(platform);
	
	}

	@Override
	public int parameterCount() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String getType() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected void resetInstruments() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void generateSignal() {
		if(Run.debug)
			System.out.println("Test Strategy, latest price = " +this.latest);
		
		Trade t = new Trade();
		t.setID(this.registeredAsset);
		t.setQuantity(1);
		t.setDirection(TradeType.BUY);
		t.setPrice(priceData.get(priceData.size()-1).close);
		t.setType(TradeType.ORDER);
		
		this.placeTrade(t);
		
	}


}
