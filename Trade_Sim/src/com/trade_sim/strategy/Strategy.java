package com.trade_sim.strategy;

import java.util.Collection;

import com.trade_sim.asset.Asset;
import com.trade_sim.asset.AssetPrice;
import com.trade_sim.platform.Platform;
import com.trade_sim.portfolio_mangement.Portfolio;


/**
 * Standard interface used to define a strategy that is compatible with the system
 * @author Samuel Palmer
 *
 */
public interface Strategy {
	
	public String getAssetID();
	public void setAsset(String asset);
	public void setAsset(Asset a);
	/**
	 * Returns the profitability of the strategy
	 * @return the log return of the strategy
	 */
	double getProfitability();
	Portfolio getPortfolio();
	/**
	 * If the strategy is allowed to place trades
	 * @return
	 */
	boolean isTradingActive();
	void setTradingActive(boolean isTradingActive);
	/**
	 * Clears all information, and resets all varbiables in the strategy. The parameters are reset to 
	 * the values given
	 * @param position
	 */
	public void reset(double[] position);
	void setPlatform(Platform platform);
	Platform getPlatform();
	/**
	 * 
	 * @return the number of available configurable parameters
	 */
	public int parameterCount();
	/**
	 * Flush the strategy with past data so the indicators are not time lagged. This can be used at the start
	 * of a simulation to allow the strategy to start trading straight away, or during to re-calibrate 
	 * the strategy
	 * @param c
	 */
	public void flush(Collection<AssetPrice> c);
	/**
	 * The type (name) of hte strategy employed
	 * @return
	 */
	public String getType();
}
