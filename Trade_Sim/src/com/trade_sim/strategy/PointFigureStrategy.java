package com.trade_sim.strategy;

import com.trade_sim.platform.Platform;
import com.trade_sim.platform.Trade;
import com.trade_sim.platform.TradeType;
import com.trade_sim.strategy.instruments.PFCharting;

public class PointFigureStrategy extends AbstractStrategy{
	
	PFCharting chart;

	/**
	 * Parameters will be the threshold value and reversal for the P&F chart
	 * @param platform
	 * @param params
	 */
	public PointFigureStrategy(Platform platform, double[] params) {
		super(platform, params);
		chart = new PFCharting((int)this.params[0],(int)this.params[1]);
	}

	@Override
	public int parameterCount() {
		return 2;
	}

	@Override
	public String getType() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected void resetInstruments() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void generateSignal() {
		//update the P&F chart
		chart.update(this.latest.close);
		//Run the trading signal generation code
		
		//if the signals given back are buy or sell generate trades:
//		if(signal == BUY){
//			
//			Trade t = new Trade();
//			t.setID(this.registeredAsset);
//			t.setQuantity(1);
//			t.setDirection(TradeType.SELL);
//			t.setPrice(this.latest.close);
//			t.setType(TradeType.ORDER);
//			
//			this.placeTrade(t);
		
//		}else if(signal == SELL){
//			Trade t = new Trade();
		
//			t.setID(this.registeredAsset);
//			t.setQuantity(1);
//			t.setDirection(TradeType.SELL);
//			t.setPrice(this.latest.close);
//			t.setType(TradeType.ORDER);
//			
//			this.placeTrade(t);
//		}
		
	}
	

}
