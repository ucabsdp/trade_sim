package com.trade_sim.strategy.instruments;

import java.util.Vector;

import com.trade_sim.asset.AssetPrice;

/**
 * Moving Average Convergence/Divergence indicator. Measures the different between two periods of exponential moving averages
 * @author Samuel Palmer
 *
 */
public class MACD implements Instrument{
	protected Instrument av1;
	protected Instrument av2;
	
	/**
	 * 
	 * @param percentage1 percentage of EMA1
	 * @param percentage2 percentage of EMA2
	 */
	public MACD (double percentage1, double percentage2){
		av1 = new ExponentialMovingAverage(percentage1);
		av2 = new ExponentialMovingAverage(percentage2);
	}
	
	/**
	 * 
	 * @param period1 Time period of EMA1
	 * @param period2 Time period of EMA2
	 */
	public MACD (int period1, int period2){
		av1 = new ExponentialMovingAverage(period1);
		av2 = new ExponentialMovingAverage(period2);
	}

	/**
	 * Subtracts the first EXPMA from the second EXPMA
	 */
	@Override
	public double calculate(Vector<AssetPrice> v) {
		return (av1.calculate(v)-av2.calculate(v));
	}

}
