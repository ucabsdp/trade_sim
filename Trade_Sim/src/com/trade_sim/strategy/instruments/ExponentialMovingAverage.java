package com.trade_sim.strategy.instruments;

import java.util.Vector;

import com.trade_sim.asset.AssetPrice;

/**
 * Exp moving average instrument, Ref Technical Analysis A-Z pg 208
 * @author Samuel Palmer
 *
 */
public class ExponentialMovingAverage implements Instrument{
	protected double percentage;
	protected double previousAverage;
	
	/**
	 * 
	 * @param percentage
	 */
	public ExponentialMovingAverage(double percentage){
		this.percentage = percentage;
	}
	
	/**
	 * 
	 * @param period
	 */
	public ExponentialMovingAverage(int period) {
		this.percentage = (2/((double)period + 1));
	}
	
	@Override 
	public double calculate(Vector<AssetPrice> v){
		double expAv = -1;
		expAv = (v.lastElement().close*percentage)+(previousAverage*(1-percentage));
		this.previousAverage = expAv;
		return expAv ;
	}

}
