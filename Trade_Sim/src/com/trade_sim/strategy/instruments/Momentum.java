package com.trade_sim.strategy.instruments;

import java.util.Vector;

import com.trade_sim.asset.AssetPrice;

/**
 * Measures the price momentum of a trend
 * Ref A-Z page 195-196
 * @author Samuel Palmer
 *
 */
public class Momentum implements Instrument{
	protected int period;
	
	public Momentum(int period){
		this.period = period;
	}
	

	@Override
	public double calculate(Vector<AssetPrice> v) {
		double mom = -1;
		int size = v.size();
		if(size >= period){
			mom = (v.lastElement().close/v.get(size-1-period).close)*100;	
		}
		return mom;
	}

}
