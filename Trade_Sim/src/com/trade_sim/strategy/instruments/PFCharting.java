package com.trade_sim.strategy.instruments;


/**
 * To use to a strategy 
 * @author samPalmer
 *
 */
public class PFCharting {
	private enum symbol{X,O, XO};
	
	protected symbol current = symbol.XO;
	protected int lastValue = 0;
	protected int reversal = 3;
	protected int threshold = 1;
	protected int reversal_count;
	protected boolean first = true;
	
	public PFCharting(int threshold, int reversal){
		this.threshold = threshold;
		this.reversal = reversal;	
	}
	
	/**
	 * Logic to generate the new P&F symbol
	 * @param price
	 */
	public void update(double price){
		int i_price = (int)price; //round price down to nearest int.
		
		if(current == symbol.X){
			int change =  i_price-lastValue;
			if((change >= threshold)){
				for(int i = 0; i<change/threshold; i++){
					addNewSymbol(symbol.X, lastValue+(i+1)*threshold);
				}
				// reset if a reversal was occuring.
				reversal_count = 0;
				
			}else if((change <= -threshold)){
				reversal_count += (-change/threshold) ;  //increase reversal count
					//if reversal count is equal to or bigger than reversal value
				if(reversal_count >= reversal){
					//add a new symbol for every reversal box
					for(int i = 0; i < reversal_count; i++){
							addNewSymbol(symbol.O, lastValue+(i+1)*threshold);
					}
				}		
			}	
		}else if(current == symbol.O){
			int change =  lastValue-i_price;
			if((change >= threshold)){
				for(int i = 0; i<change/threshold; i++){
					addNewSymbol(symbol.O, lastValue+(i+1)*threshold);
				}
				// reset if a reversal was occuring.
				reversal_count = 0;
				
			}else if((change <= -threshold)){
				reversal_count += (-change/threshold) ;  //increase reversal count
					//if reversal count is equal to or bigger than reversal value
				if(reversal_count >= reversal){
					//add a new symbol for every reversal box
					for(int i = 0; i < reversal_count; i++){
							addNewSymbol(symbol.X, lastValue+(i+1)*threshold);
					}
				}		
			}
		}else if(current == symbol.XO){
			//determine the initial column.
			int change =  i_price-lastValue;
			if(first){
				lastValue = i_price;
				first = !first;
			}else if(change >= threshold){
				for(int i = 0; i<change/threshold; i++){
					addNewSymbol(symbol.X, lastValue+(i+1)*threshold);
				}
			}else if(change <= -threshold){
				for(int i = 0; i < reversal_count; i++){
					addNewSymbol(symbol.O, lastValue+(i+1)*threshold);
				}
			}
		}
		
	}
	
	/**
	 * Adds new symbol to the P&F plot segment
	 * @param s
	 */
	protected void addNewSymbol(symbol s, int price){
		this.lastValue = price;
		
		//generate the P&F chart segment
		
		current = s;
	}
		
}
