package com.trade_sim.strategy.instruments;

import java.util.Vector;

import com.trade_sim.asset.AssetPrice;

/**
 * Calculates the standard deviation of close prices from given time series data
 * @author Samuel Palmer
 *
 */
public class StandardDeviation extends SimpleMovingAverage{
	protected int period;

	/**
	 * 
	 * @param period
	 */
	public StandardDeviation(int period){
		super(period);
		this.period = period;
	}

			
			
	@Override
	public double calculate(Vector<AssetPrice> v) {
			double sma = super.calculate(v);
			double sDev = -1;
			if(sma > 0){
			double sum = 0;
			for(int i = windowStart; i<=windowEnd; i++){
				sum += Math.pow((v.get(i).close - sma), 2) ;
			}
			sDev = Math.sqrt(sum/(double)period);
			}
			
			
		return sDev;
	}

}
