package com.trade_sim.strategy.instruments;

import java.util.Vector;

import com.trade_sim.asset.AssetPrice;

/**
 * Calculates the simple moving average (SMA) of close prices for a given set of asset price data
 * @author Samuel Palmer
 *
 */
public class SimpleMovingAverage implements Instrument{
	protected int windowSize = 0;
	protected int windowStart = 0;
	protected int windowEnd = 0;
	
	
	public SimpleMovingAverage(int windowSize) {
		super();
		this.windowSize = windowSize;
	}
	/**
	 * @return the windowSize
	 */
	public int getWindowSize() {
		return windowSize;
	}
	/**
	 * @param windowSize the windowSize to set
	 */
	public void setWindowSize(int windowSize) {
		this.windowSize = windowSize;
	}
	
	@Override
	public double calculate(Vector<AssetPrice> v) {
		
		this.windowEnd = v.size()-1;
		this.windowStart = windowEnd-(windowSize-1);
		double total = 0;
		
		
		if(!(windowStart < 0) && windowSize>0){
			
		
			for(int i = windowStart; i<=windowEnd; i++){
			total += v.get(i).close;
			}
			double average= 0;
			
		
		
		try{
		 average = total/windowSize;
		}catch(Exception e){e.printStackTrace(); average = -1;}
		return average;	
		}else{
			return -1;
		}
	
	}

}
