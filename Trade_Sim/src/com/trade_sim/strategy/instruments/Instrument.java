package com.trade_sim.strategy.instruments;
import java.util.Vector;

import com.trade_sim.asset.AssetPrice;


public interface Instrument {
	
	public double calculate(Vector<AssetPrice> v);

}
