package com.trade_sim.strategy;

import java.util.Arrays;

import java.util.Collection;
import java.util.Vector;

import com.trade_sim.asset.Asset;
import com.trade_sim.asset.AssetListener;
import com.trade_sim.asset.AssetPrice;
import com.trade_sim.asset.Position;
import com.trade_sim.framework.Loggable;
import com.trade_sim.framework.Run;
import com.trade_sim.platform.Platform;
import com.trade_sim.platform.Trade;
import com.trade_sim.portfolio_mangement.Portfolio;

/**
 * General template for a strategy. Implements all underlying utility methods
 * @author Samuel Palmer
 *
 */
public abstract class AbstractStrategy extends Loggable implements AssetListener, Strategy{
	
	protected int buySignals;
	protected int sellSignals;
	protected Platform platform;
	protected String StrategyName = this.getClass().getCanonicalName();
	protected Vector<AssetPrice> priceData = new Vector<AssetPrice>();
	protected AssetPrice latest;
	protected String registeredAsset;
	protected Portfolio p = new Portfolio();
	protected boolean storeHistory;
	protected boolean active = true;
	protected boolean isTradingActive = true;
	
	protected double[] params;
	
	@Override
	public void reset(final double[] params){
		if(params == null){
			this.params = new double[this.parameterCount()];
			Arrays.fill(this.params, 0);
		}else{
		this.params = params;
		}
		buySignals = 0;
		this.sellSignals = 0;
		this.priceData = new Vector<AssetPrice>();
		this.latest = null;
		this.p = new Portfolio();
		this.resetInstruments();
	}
	
	/**
	 * 
	 * @param platform
	 */
	public AbstractStrategy(Platform platform){
		this(platform, null);
	}
	
	/**
	 * If parameters are null then they are all set to a default of 0
	 * @param platform
	 * @param params parameters to use
	 */
	public AbstractStrategy(Platform platform, final double[] params){
		if(params == null){
			this.params = new double[this.parameterCount()];
			Arrays.fill(this.params, 0);
			System.out.println(Arrays.toString(this.params));
		}else{
		this.params = params;
		}
		this.platform = platform;	;
		Run.log.log("Strategy "+this.getClass().getCanonicalName() +" created");
	}
	
	abstract protected void resetInstruments();
	
	@Override
	public void update(AssetPrice price) {
		if(active){
			
			priceData.add(price);
			latest = price;
			generateSignal();
		if(Run.debug)
			System.out.println("New price " +priceData.get(priceData.size()-1).toString());
		
		}
	}
	
	/**
	 * Turn this strategy on to receive price updates and generate corresponding trading signals 
	 * @param active
	 */
	public void setActive(boolean active){
		this.active = active;
	}

	/**
	 * Algorithm to generate trading signals for this strategy. Called internally by update();
	 */
	protected abstract void generateSignal();
	
	@Override
	public Platform getPlatform() {
		return platform;
	}
	
	@Override
	public void setPlatform(Platform platform) {
		this.platform = platform;
	}

	@Override
	public void setAsset(String asset){
		this.registeredAsset = asset;
	}
	
	@Override
	public void setAsset(Asset a) {
		this.registeredAsset = a.getID();
		a.addListener(this);
	}
	
	@Override
	public String getAssetId() {
		// TODO Auto-generated method stub
		return this.registeredAsset;
	}

	@Override
	public String getAssetID() {
		// TODO Auto-generated method stub
		return this.registeredAsset;
	}
	
	@Override
	public double getProfitability(){
		this.log.log(this.toString());
		if(latest !=null)
		return p.calculateReturn(latest);
	//	return p.calculatePL(latest);
		else
			return -1;
		
	}
	@Override
	public Portfolio getPortfolio() {
		return p;
	}

	public void setP(Portfolio p) {
		this.p = p;
	}

	@Override
	public boolean isTradingActive() {
		return isTradingActive;
	}
	
	@Override
	public void flush(Collection<AssetPrice> c){
		this.setTradingActive(false);
		for(AssetPrice p : c){
		this.update(p);
		}
		this.setTradingActive(true);
	}
	
	@Override
	public void setTradingActive(boolean isTradingActive) {
		System.out.println("CHANGE IN STATE");
		this.isTradingActive = isTradingActive;
	}
	
	/**
	 * Places a trade to the registerd platform. If the trade is successful it adds the position
	 * to the portfolio
	 * @param t
	 */
	protected void placeTrade(Trade t){
		if(this.isTradingActive){
		 Position pos = platform.openPosition(t);
			if( pos != null){
				p.addPosition(pos);
			}
		}
	}
	
	@Override
	protected String getLoggableState() {
		StringBuilder sb = new StringBuilder();
		sb.append("Active, " +this.isTradingActive);
		sb.append("Trading Asset, " +this.registeredAsset);
		sb.append("Profitability, " +this.getProfitability());
		sb.append("Buy Signals, " +this.buySignals);
		sb.append("Sell Signals, " +this.sellSignals);
		return sb.toString();
	}

}
