package com.trade_sim.strategy;

import java.util.Arrays;

import com.trade_sim.framework.Setting;


/**
 * Bean class used to contain configurable strategy settings.
 * @author Samuel Palmer
 *
 */
public class StrategySettings extends Setting{
	
	public String name;
	public String strategy;
	public String tradedAsset;
	public double[] params;
	public String type;
	
	@Override
	public String toString() {
		return "StrategySettings [name=" + name + ", strategy=" + strategy
				+ ", tradedAsset=" + tradedAsset + ", params="
				+ Arrays.toString(params) + ", type=" + type + "]";
	}
	
	
	

}
