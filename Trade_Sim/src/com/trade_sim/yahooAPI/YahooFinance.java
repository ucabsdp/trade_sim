package com.trade_sim.yahooAPI;

import java.io.BufferedReader;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.Collection;
import java.util.Vector;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

import com.trade_sim.asset.AssetPrice;

/**
 * Used to query the Yahoo!Finance API
 * @author Samuel Palmer
 *
 */
public class YahooFinance {
	final static private String top, beginAs, endAs, space, endSpace, fields, space2;
	static{
	top = "http://query.yahooapis.com/v1/public/yql?q=select%20";
	beginAs = "%20from%20yahoo.finance.quotes%20where%20symbol%20in%20(";
	//)%0A%20%0A%09%09
	endAs = "&env=http%3A%2F%2Fdatatables.org%2Falltables.env" ;
	space = "%22";
	space2 = "%20";
	endSpace = "%2C";
	fields = "*";
	//http://query.yahooapis.com/v1/public/yql?q=select%20*%20
	//from%20yahoo.finance.quotes%20where%20symbol%20in%20
	//(%22^FTSE%22)%0A%09%09&env=http%3A%2F%2Fdatatables.org%2Falltables.env
	//fields = "Ask"+endSpace+space2+"Bid";
	}
	
	/**
	 * 
	 * @param assets, collection of asset tickers to obtain data for
	 * @param select the asset information to be selected
	 * @return A collection of asset price data for the given query
	 */
	public Collection<AssetPrice> Query(String[] assets, String[] select){
		Vector<AssetPrice> prices = new Vector<AssetPrice>();
		
		
		YahooQuery q = new YahooQuery();
		q.bindSelect(select);
		q.bindAssets(assets);
		String query = q.getQuery();
		
		try {
			URI uri = new URI("http","query.yahooapis.com","/v1/public/yql","q="+query,null);
			URL url = uri.toURL();
	
		SAXParserFactory spf = SAXParserFactory.newInstance();
		SAXParser saxParser = spf.newSAXParser();
		XMLReader xmlReader = saxParser.getXMLReader();
		YahooFinanceHandeler yfh = new YahooFinanceHandeler();
		xmlReader.setContentHandler(yfh);
		xmlReader.parse(new InputSource(uri.toASCIIString()+endAs));
		prices = yfh.getPrices();
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return prices;
	}

}
