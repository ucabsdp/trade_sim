package com.trade_sim.yahooAPI;

/**
 * Parameters available to be obtained from the Yahoo!Finance API
 * @author Samuel Palmer
 *
 */
public enum YahooSelect {
	Ask, Bid, LastTradePriceOnly

}
