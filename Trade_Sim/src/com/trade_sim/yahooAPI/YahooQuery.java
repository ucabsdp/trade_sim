package com.trade_sim.yahooAPI;

/**
 * Builds the Yahoo!Finance API query
 * @author Samuel Palmer
 *
 */
public class YahooQuery {
	
	 private String query = "select PARAMS1 FROM yahoo.finance.quotes where symbol in (PARAMS2)";
	
	 /**
	  * Binds the select fields to the query
	  * @param select
	  */
	public void bindSelect(String[] select){
		StringBuffer bf = new StringBuffer();
		int i = 0;
		for(String s : select){
			bf.append(s);
			i++;
			if(i != select.length){
				bf.append(",");
			}
		}
		this.query = query.replaceFirst("PARAMS1", bf.toString());
	}
	
	/**
	 * Binds the asset fields to the query
	 * @param assets
	 */
	public void bindAssets(String[] assets){
		StringBuffer bf = new StringBuffer();
		int i = 0;
		for(String as : assets){
			bf.append("\"");
			as.toUpperCase();
			//bf.append(space);
			bf.append(as);
			bf.append("\"");
			i++;
		//	bf.append(space);
			if(i != assets.length){
				bf.append(",");
			}
		}
		this.query = query.replaceFirst("PARAMS2", bf.toString());
	}
	
	/**
	 * 
	 * @return the corresponding API query
	 */
	public String getQuery(){
		return this.query;
	}

}
