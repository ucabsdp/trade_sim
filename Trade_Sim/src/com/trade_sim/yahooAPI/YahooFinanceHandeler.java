package com.trade_sim.yahooAPI;

import java.util.Arrays;

import java.util.Vector;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.trade_sim.asset.AssetPrice;


/**
 * Parses the Yahoo!Finance XML response
 * @author Samuel Palmer
 *
 */
public class YahooFinanceHandeler extends DefaultHandler{
	private AssetPrice price; 
	private StringBuffer sb = new StringBuffer();
	private Vector<AssetPrice> prices = new Vector<AssetPrice>();
	
	@Override public void startElement(String uri, String localName,
			String qName,Attributes attributes) throws SAXException {
		if(qName == "quote"){
			price = new AssetPrice();
			price.ID = attributes.getValue("symbol");
		}
		
		System.out.println(uri);
		
	}
	
	@Override public void endElement(String uri, String localName, String
			qName) throws SAXException {
		
		//improve by using reflection price.setfield, with the name....
		
		if(qName == "Ask"){
			try{
			price.ask = Double.parseDouble(sb.toString());
			}catch(Exception e){
			price.ask = 0.00;
			}
		}
		
		if(qName == "Bid"){
			try{
			price.bid = Double.parseDouble(sb.toString());
			}catch(Exception e){
			price.bid = 0.00;
			}
		}
		
		if(qName == YahooSelect.LastTradePriceOnly.toString()){
			try{
			price.close = Double.parseDouble(sb.toString());
			}catch(Exception e){
			price.close = 0.00;
			}
		}
		
		if(qName == "quote"){
			prices.add(price);
		}
		
	}
	
	@Override 
	public void characters(char[] ch, int start, int length) throws SAXException {
		sb = new StringBuffer();
		sb.append(ch, start, length);
	}

	public Vector<AssetPrice> getPrices() {
		return prices;
	}

	
}
