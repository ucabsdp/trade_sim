package com.trade_sim.asset;

public interface AssetListener {
	
	/**
	 * Update the object when new price information arrives
	 * @param price
	 */
	void update(AssetPrice price);
	/**
	 * Gets the associated asset ID for this object
	 * @return
	 */
	public String getAssetId();

}
