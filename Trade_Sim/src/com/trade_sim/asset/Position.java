package com.trade_sim.asset;

import com.trade_sim.framework.CSVWriteable;
import com.trade_sim.framework.GLOBAL;
import com.trade_sim.platform.Trade;
import com.trade_sim.platform.TradeType;



/**
 * Contains information about a position held in an asset.Whilst this position is open 
 * the current price of the asset can be updated, and the latest profit/return calculated. If the position
 * is closed the current price will be that of the price it was closed at.
 * @author Samuel Palmer
 *
 */
public class Position implements Comparable, CSVWriteable{
	
	@Override
	public String toString() {
		return "Position [assetID=" + assetID + ", quantity=" + quantity
				+ ", currentValue=" + currentValue 
				+ ", openAssetValue=" + openAssetValue + ", currentAssetValue="
				+ currentAssetValue + ", profit=" + profit + ", direction="
				+ direction + "]";
	}

	protected String assetID;
	protected int quantity;
	/**
	 * Current asset price(value)
	 */
	protected double currentValue;
	/**
	 * Original price of the asset with the position was opened
	 */
	protected double openAssetValue;
	protected double currentAssetValue;
	protected double profit;
	protected TradeType direction;
	protected int executionTime;
	protected double transactionCost;
	protected boolean closed = false;
	
	public double getTransactionCost() {
		return transactionCost;
	}

	public Position(String assetID, int quantity, double openAssetValue, TradeType direction, int executionTime, double transactionCost){
		this.assetID = assetID;
		this.quantity = quantity;
		this.openAssetValue = openAssetValue;
		this.direction = direction;
		this.executionTime = executionTime;
		this.transactionCost = transactionCost;
	}
	
	
	public static Position newPosition(Trade t){
		return new Position(t.getID(), t.getQuantity(), t.getPrice(), t.getDirection(), GLOBAL.getTimeStep(), 0);
	}

	public double update(AssetPrice price) {
		currentValue = price.close;
		profit = this.calculateProfit(price.close);
		return this.profit;
	
//		System.out.println("New price postion " +price.toString()); 
		//System.out.println("Position open at " +openAssetValue +" P&L : " +profit );
	}
	
	public double calculateProfit(double price){
		return (direction.getValue()*(price - openAssetValue)*this.quantity)-this.transactionCost;
	}
	
	/**
	 * Calculates the return of the position. It is transaction cost adjusted.
	 * @return
	 */
	public double calculateReturn(){
		return (this.profit/this.openAssetValue)*100;
	}
	
	/**
	 * Calculates the cost of the position.
	 */
	public double cost(){
		return (this.openAssetValue*this.quantity)+this.transactionCost;
	}
	
	/**
	 * Attempts to close the position with position p. If it can be closed an excess position will be returned 
	 * if they differ on quantity or null if the position is directly closed. This sets the closed state of the position
	 * to true.
	 * @param p Closing position
	 * @return Excess position after closure, or null if no excess.
	 * @throws Exception If the position cannot be closed
	 */
	public Position close(Position p) throws Exception{
		if(p.assetID.equals(this.assetID) && p.getDirection() != this.direction){
		this.currentValue = p.openAssetValue;
		this.closed = true;
		this.update(null);
		//return the excess position
		if(p.quantity > this.quantity){
			return new Position(this.assetID,p.quantity-this.quantity, p.openAssetValue, p.direction,p.executionTime,p.transactionCost);
		}else if(this.quantity > p.quantity){
			return new Position(this.assetID,this.quantity-p.quantity, this.openAssetValue, this.direction,this.executionTime,this.transactionCost);
		}else return null;
		}
		throw new Exception("Position cannot be closed");
	}
	
	

 	public String getAssetId() {
		return assetID;
	}
	
	public int getQuantity() {
		return quantity;
	}

	public double getCurrentValue() {
		return currentValue;
	}

	public double getOpenAssetValue() {
		return openAssetValue;
	}

	public double getCurrentAssetValue() {
		return currentAssetValue;
	}

	public double getProfit() {
		return profit;
	}

	public TradeType getDirection() {
		return direction;
	}

	public static Position newPosition(Trade t, int timeStep, double transactionCost) {
		return new Position(t.getID(), t.getQuantity(), t.getPrice(), t.getDirection(), GLOBAL.getTimeStep(), transactionCost);
	}

	public int getExecutionTime() {
		return this.executionTime;
	}

	@Override
	public int compareTo(Object arg0) {
		
		return 0;
	}

	@Override
	public String toCSV() {
		return (this.assetID +"," +this.executionTime + "," +this.openAssetValue +"," +this.direction.toString() +"," +this.quantity);
	
	}
	
	public static String CSVHeadings(){
		return ("AssetID, Execution Time, Open Asset Value, Direction, Quantity");
	}

}
