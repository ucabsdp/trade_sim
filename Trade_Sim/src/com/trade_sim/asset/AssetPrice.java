package com.trade_sim.asset;

import com.trade_sim.framework.CSVWriteable;



/**
 * A container class for all the asset price information
 * @author Samuel Palmer
 *
 */
public class AssetPrice implements CSVWriteable{
	public String ID;
	public double price;
	public double bid;
	public double ask;
	public double open;
	public double close;
	public double volume;
	
	@Override
	public String toString(){
		return (ID+ "," +price +"," +bid +","+ask+","+open+","+close+","+volume);
	}

	@Override
	public String toCSV() {
		
		return null;
	}
	
}
