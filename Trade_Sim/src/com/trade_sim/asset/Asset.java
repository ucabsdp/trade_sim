package com.trade_sim.asset;

import java.util.HashMap;


import java.util.Collection;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.Vector;
import java.util.WeakHashMap;

import com.trade_sim.framework.Run;


/**
 * This class is used to update all objects that listen for a change in new information for a particular asset. It also contains a collection of live history data for the 
 * asset.
 * A weak listener implementation is used, therefore de-registering a listener is not an issue, and objects are automatically removed from the list if they are garbage collected.
 * @author Samuel Palmer
 *
 */
public class Asset{
	
	public static boolean isLiveHistory;	
	protected AssetPrice price;
	protected String ID;
	protected Vector<AssetPrice> liveHistory = new Vector<AssetPrice>();
	protected  WeakHashMap<AssetListener, String> al = new WeakHashMap<AssetListener, String>(10);
	
	/**
	 * 
	 * @param ID asset ID
	 */
	public Asset(String ID){
		this.ID = ID;
	}
	
	/**
	 * Get the assets ticker ID
	 * @return
	 */
	public String getID(){
		return this.ID;
	}
	
	/**
	 * Update the asset with new price information. This then updates all listeners
	 * @param price
	 */
	public void updatePrice(final AssetPrice price){
		this.price = price;
		if(isLiveHistory)
		liveHistory.add(this.price);
		notifyListeners();
	}
	
	/**
	 * 
	 * @return all the price history collected by this asset
	 */
	public Collection<AssetPrice> getliveHistory(){
		return this.liveHistory;
	}
	
	protected synchronized void notifyListeners(){
		
		
		for(Object e : al.entrySet().toArray()){

			((AssetListener)((Entry)e).getKey()).update(price);
		}
		if(Run.debug)
		System.out.println("Listener size " +al.size());
		
//		Iterator i = al.entrySet().toArray();
//		while(i.hasNext()){
//			((AssetListener)((Entry)i.next()).getValue()).update(price);
//		}
	}
	
	public synchronized void addListener(final AssetListener al){
	
		this.al.put(al, "DUMMY");
	}
	
	public synchronized void addLowPrioListener(final AssetListener al){
		
		//this.allp.put(Integer.toString(this.allp.size()), al);
	}

}


