package com.trade_sim.market;

import java.io.BufferedReader;



import java.io.FileReader;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;

import com.trade_sim.asset.AssetPrice;
import com.trade_sim.framework.GLOBAL;
import com.trade_sim.framework.Run;


public class SequentialCSVMarket implements Market{
	protected boolean parseDate = false;
	protected MarketObserver observer;
	protected String fileName = "Empty";
	protected AssetPrice currentPrice;
	protected BufferedReader reader;
	protected String name;
	protected MarketAccess ma;
	protected int size = 0;
	
	public SequentialCSVMarket (String fileName){
		if(!fileName.isEmpty())
		this.fileName = fileName;
		try {
			ma = new SequentialMarketAccess(this);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		//Find the file size
		try{
			this.reader = new BufferedReader(new FileReader(fileName));	
			while(reader.ready()){
				reader.readLine();
				size++;
			}
			}catch(Exception e){Run.log.log("Market source corrupted, file may not exist");}
	}
	
	
	/**
	 * 
	 * @return The number of data points in this market
	 */
	@Override
	public int getSize() {
		return size;
	}



	@Override
	public void addObserver(MarketObserver observer) {
		this.observer = observer;
	}



	@Override
	public void connect() throws Exception {
		GLOBAL.resetTimeStep();
		try{
		this.reader = new BufferedReader(new FileReader(fileName));	
		//reads in first formating line
		if(reader.ready())
			reader.readLine();
		}catch(Exception e){Run.log.log("Conection to market failed"); throw e;}
	}



	@Override
	public void disconnect() {
		// TODO Auto-generated method stub
		
	}
	
	protected AssetPrice initPrice(String assetData){
		String[] tempData = assetData.split(",");
		
		if(parseDate){
		DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		try {
			GLOBAL.setCurrentSimulationTime(formatter.parse(tempData[0]));
		} catch (ParseException e) {
			System.out.println("Date not parsed");
		}
		}
		
		AssetPrice asset = new AssetPrice();
		asset.open = Double.parseDouble(tempData[1]);
		asset.close = Double.parseDouble(tempData[4]);
		asset.ID = fileName;
		return asset;
		
	}


	@Override
	public void getNewPrice() throws Exception{
		String line = null;
		if(reader.ready()){
			line = reader.readLine();
			
			currentPrice = initPrice(line);
			if(Run.debug)
				System.out.println("New Price for CSV = " +currentPrice);
				
		}else{
			if(Run.debug)
				System.out.println("Stream empty");
			currentPrice = null;
			
		}
		
		this.observer.update();
		GLOBAL.incrementTimeStep();
	}

	@Override
	public AssetPrice getCurrentPrice() throws Exception {
		return this.currentPrice;
	}



	public boolean isParseDate() {
		return parseDate;
	}


	@Override
	public void setParseDate(boolean parseDate) {
		this.parseDate = parseDate;
	}



	@Override
	public MarketAccess getMarketAccess() {
		return ma;
	}
	
	@Override
	public String toString(){
		return this.fileName;
	}



	@Override
	public String getAsset() {
		return fileName;
	}
}
