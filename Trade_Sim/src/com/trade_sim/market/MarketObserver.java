package com.trade_sim.market;

import com.trade_sim.asset.AssetPrice;



/**
 * Used to directly listen to the market source {@link Market}
 * @author Samuel Palmer
 *
 */
public interface MarketObserver {
	
	public void update();

	public void update(AssetPrice currentPrice);

}
