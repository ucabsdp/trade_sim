package com.trade_sim.market;

/**
 * Builds a market implementation from given {@link MarketSettings}. It currently builds either a
 * Yahoo!Finance live market {@link YahooMarket} or a CSV file market {@link SequentialCSVMarket}
 * @author Samuel Palmer
 *
 */
public class MarketBuilder {
	
	
	public static Market build(MarketSettings settings){
		
		switch(settings.type){
		
		case CSV : return buildCSV(settings) ;
		case YahooFinance: return buildYahoo(settings) ; 
		default : return null;
		}
	}
	
	private static Market buildYahoo(MarketSettings settings) {
		Market m = new YahooMarket(settings.source, settings.selection.split(","));
	//	m.setParseDate(settings.parseDate);
		return m;
	}

	private static Market buildCSV(MarketSettings settings) {
		 Market m = new SequentialCSVMarket(settings.source);
		 m.setParseDate(settings.parseDate);
		 return m;
	}

}
