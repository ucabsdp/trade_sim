package com.trade_sim.market;

import com.trade_sim.asset.AssetPrice;
import com.trade_sim.framework.Run;



/**
 * 
 * @author Samuel Palmer
 * 
 * The SequentialMarketAccess class acts as an adapater between the platform and market
 * which allows for certain behavior only seen in this model.
 * - All trades made at the current price will be garenteed at the current market price
 *
 */

public class SequentialMarketAccess extends MarketAccess {
	protected Market market;
	protected AssetPrice currentPrice;
	protected AssetPrice priceBuffer;
	protected boolean streamActive;
	protected String name;
	
	/**
	 * @return the streamActive
	 */
	@Override
	public boolean isStreamActive() {
		return streamActive;
	}

	/**
	 * @param streamActive the streamActive to set
	 */
	@Override
	public void setStreamActive(boolean streamActive) {
		this.streamActive = streamActive;
	}

	public SequentialMarketAccess(SequentialCSVMarket market) throws Exception{
		this.market = market;
		market.addObserver(this);
		Run.log.log("Connected to CSV Market " +market.fileName);
	}
	
	/**
	 * Creates a sequential csv market using the given file
	 * @param fileName
	 * @throws Exception
	 */
	public SequentialMarketAccess(String fileName) throws Exception{
		this.market = new SequentialCSVMarket(fileName);
		market.addObserver(this);
		Run.log.log("Connected to CSV Market " +fileName);
	}
	
	@Override
	public Market getMarket(){
		return this.market;
	}
	
	@Override
	public void connect() throws Exception {
		market.connect();
		this.streamActive = true;
		Run.log.log("Connected to CSV Market ");
	}
	
	@Override
	public void update() {
		try {
			currentPrice = market.getCurrentPrice();
			if(currentPrice !=null){ //if not null, check if any assets need to be updated with this info
				if(registeredAssets.containsKey(currentPrice.ID))
			registeredAssets.get(currentPrice.ID).updatePrice(currentPrice);
			}else{ //if the price is null end the stream and close the market as there is an error
				if(Run.debug)
					Run.log.log("Null price stream ");
				streamActive = false;
			}
		} catch (Exception e) {
			currentPrice = null;
			streamActive = false;
			Run.log.log("Error updating price from market ");
			e.printStackTrace();
		}
	}

	@Override
	public AssetPrice getQuote(String ID) {
		//This single asset market does not use the passed ID;	
		try {
		//	Run.log.log("Getting quote for " +ID);
			return currentPrice;
		} catch (Exception e) {
			Run.log.log("Getting quote for " +ID +" Error thrown " +e.getMessage());
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public void disconnect() {		
	}

	@Override
	public void update(AssetPrice currentPrice) {

	}

	
	
}
