package com.trade_sim.market;

import com.trade_sim.asset.AssetPrice;

/**
 * Provides access to the live Yahoo!Finance market stream {@link YahooMarket}
 * @author Samuel Palmer
 *
 */
public class YahooMarketAccess extends MarketAccess{
	private YahooMarket market;
	
	public YahooMarketAccess(YahooMarket market) throws Exception{
		this.market = market;
		market.addObserver(this);
	}

	@Override
	public Market getMarket() {
		return this.market;
	}

	@Override
	public AssetPrice getQuote(String ID) {
		return null;
	}

	@Override
	public void connect() throws Exception {
		this.market.connect();
		this.streamActive = true;
	}

	@Override
	public void disconnect() {
		this.market.disconnect();
		this.streamActive = false;
		
	}

	@Override
	public void update() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(AssetPrice currentPrice) {
		System.out.println(currentPrice.ID);
		try{
		if(this.registeredAssets.containsKey(currentPrice.ID)){
			this.registeredAssets.get(currentPrice.ID).updatePrice(currentPrice);
		}
		}catch(Exception e){
			e.printStackTrace();
			disconnect();
		}
		
	}

}
