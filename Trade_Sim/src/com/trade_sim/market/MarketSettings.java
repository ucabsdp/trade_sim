package com.trade_sim.market;

import com.trade_sim.framework.Setting;



public class MarketSettings extends Setting{
	
	public String name;
	public MarketType type;
	public boolean parseDate;
	public String source;
	public String selection;
	
	@Override
	public String toString() {
		return "MarketSettings [name=" + name + ", type=" + type
				+ ", parseDate=" + parseDate + ", source=" + source
				+ ", selection=" + selection + "]";
	}

}
