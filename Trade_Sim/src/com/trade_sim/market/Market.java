package com.trade_sim.market;

import com.trade_sim.asset.AssetPrice;


/**
 * The interface defined for a market class to be compatible with the system
 * @author Samuel Palmer
 *
 */
public interface Market {
	
	public void connect() throws Exception;
	public void disconnect();
	public AssetPrice getCurrentPrice() throws Exception;
	public void addObserver(MarketObserver observer);
	/**
	 * This method is vital - it is called by the platform to either start price iteration or activate the price thread
	 * @throws Exception
	 */
	void getNewPrice() throws Exception;
	void setParseDate(boolean parseDate);
	public MarketAccess getMarketAccess();
	
	String getAsset();
	/**
	 * If possible returns the size of the market data, else null is returned
	 * @return
	 */
	int getSize();

}
