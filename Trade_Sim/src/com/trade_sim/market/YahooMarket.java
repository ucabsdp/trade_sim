package com.trade_sim.market;

import java.util.Arrays;

import com.trade_sim.asset.AssetPrice;
import com.trade_sim.framework.ThreadPool;
import com.trade_sim.yahooAPI.YahooFinance;


/**
 *  *Uses Yahoo!Finance to poll market prices. This then pushes the polled prices to the system.
 *  This uses the Yahoo!Finance library {@link YahooFinance}
 * @author Samuel Palmer
 */
public class YahooMarket extends Thread implements Market {
	final public MarketType type = MarketType.YahooFinance;
	protected boolean isRunning;
	protected YahooFinance yf = new YahooFinance();
	protected MarketAccess ma ;
	protected MarketObserver mo;
	protected AssetPrice currentPrice;
	protected String ID;
	protected long period = 60000;
	protected String[] assets; 
	protected String[] select;
	
	/**
	 * Single asset  market obtained from the Yahoo!Finance API
	 * @param asset The asset to be viewed
	 * @param select Array of parameters to obtain, {@link YahooSelect}
	 */
	public YahooMarket(final String asset, String[] select){
		super(ThreadPool.getSimulationGroup(), "YahooMarketThread");
		
		this.assets = new String[]{asset};
		this.select = select;
		try {
			ma = new YahooMarketAccess(this);
		} catch (Exception e) {
			e.printStackTrace();
		};
	}

	@Override
	public void connect() throws Exception {
		this.isRunning = true;
	}

	@Override
	public void disconnect() {
		this.isRunning = false;
	}

	@Override
	public AssetPrice getCurrentPrice() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void addObserver(final MarketObserver observer) {
		mo = observer;
	}
	@Override
	public void run(){
		while(isRunning){
			currentPrice = (AssetPrice)yf.Query(this.assets, this.select).toArray()[0];
			currentPrice.ID = this.assets[0];
			mo.update(currentPrice);
			try {
				Thread.sleep(period);
			} catch (InterruptedException e) {
				e.printStackTrace();
				Thread.currentThread().interrupt();
				break;
			}
		}	
		currentPrice = null;
		mo.update(null);
		
	}

	@Override
	public void getNewPrice() throws Exception {
		
			this.run();
		
//		while(isRunning){
//			System.out.println("Connected");
//			currentPrice = (AssetPrice)yf.Query(this.assets, this.select).toArray()[0];
//			mo.update(currentPrice);
//		}	
//		currentPrice = null;
//		mo.update(null);
	}

	public long getPeriod() {
		return period;
	}

	/**
	 * Set the price polling period in milliseconds
	 * @param period
	 */
	public void setPeriod(long period) {
		this.period = period;
	}

	@Override
	public void setParseDate(boolean parseDate) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public MarketAccess getMarketAccess() {
		return this.ma;
	}

	@Override
	public String toString() {
		return "YahooMarket [ID=" + ID + ", assets=" + Arrays.toString(assets)
				+ ", select=" + Arrays.toString(select) + "]";
	}
	
	@Override
	public String getAsset(){
		return this.assets[0];
	}

	@Override
	public int getSize() {
		// TODO Auto-generated method stub
		return 0;
	}
	
	
	

}
