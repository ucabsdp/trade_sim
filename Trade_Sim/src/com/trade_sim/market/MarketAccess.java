package com.trade_sim.market;

import java.util.HashMap;

import com.trade_sim.asset.Asset;
import com.trade_sim.asset.AssetListener;
import com.trade_sim.asset.AssetPrice;


/**
 * This class provides access to the underlying market implementation. The market itself should not be accessed directly, as this class provides additional functionality 
 * and a level of abstraction.
 * @author Samuel Palmer
 *
 */
public abstract class MarketAccess implements MarketObserver{
	public HashMap<String, Asset> registeredAssets = new HashMap<String, Asset>() ;
	public abstract Market getMarket();
	public abstract AssetPrice getQuote(String ID);
	public abstract void connect() throws Exception;
	public abstract void disconnect();
	protected boolean streamActive;
	
	/**
	 * Registers a listener for the asset of given ID
	 * @param ID asset to listen for
	 */
	public void registerAsset(String ID){
		if(!registeredAssets.containsKey(ID)){
			registeredAssets.put(ID, new Asset(ID));
		}
	}
	
	/**
	 * Registers a new assetlistener to the market.
	 * @param a
	 */
	public void registerAsset(AssetListener a) {
		if(!registeredAssets.containsKey(a.getAssetId())){
			registeredAssets.put(a.getAssetId(), new Asset(a.getAssetId()));
		}
		registeredAssets.get(a.getAssetId()).addListener(a);
	}
	
	/**
	 *Sets the current market stream active 
	 * @param streamActive
	 */
	public void setStreamActive(boolean streamActive) {
		this.streamActive = streamActive;	
	}
	
	/**
	 * Checks if the current market stream is active
	 * @return
	 */
	public boolean isStreamActive() {
		return this.streamActive;
	}
	
	/**
	 * Registers an assetlistener with low priority. A low priority listener gets updated last when new price information arrives 
	 * @param a
	 */
	@Deprecated
	public void registerAssetLp(AssetListener a) {
		if(!registeredAssets.containsKey(a.getAssetId())){
			registeredAssets.put(a.getAssetId(), new Asset(a.getAssetId()));
		}
		registeredAssets.get(a.getAssetId()).addLowPrioListener(a);
		
	}

}
