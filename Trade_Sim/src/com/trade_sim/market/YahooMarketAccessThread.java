package com.trade_sim.market;

import java.util.concurrent.BlockingQueue;

import java.util.concurrent.LinkedBlockingQueue;

import com.trade_sim.asset.AssetPrice;

/**
 * This runs in a seperate thread, and simulates a live stream of market data by polling the Yahoo!Finance API at regular time intervals
 * @author Samuel Palmer
 *
 */
public class YahooMarketAccessThread extends MarketAccess implements Runnable{
	protected Market market;
	 protected BlockingQueue<AssetPrice> pq = new LinkedBlockingQueue<AssetPrice>(10000);
	
	public YahooMarketAccessThread(){
	
	}
	

	@Override
	public void update() {
	}

	@Override
	public void update(AssetPrice currentPrice) {
		pq.add(currentPrice);
	}

	@Override
	public void run() {
		while(this.streamActive){
			AssetPrice currentPrice = pq.poll();
			System.out.println(currentPrice.ID);
			try{
			if(this.registeredAssets.containsKey(currentPrice.ID)){
				this.registeredAssets.get(currentPrice.ID).updatePrice(currentPrice);
			}
			}catch(Exception e){
				e.printStackTrace();
				disconnect();
			}
		}
	}

	@Override
	public Market getMarket() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AssetPrice getQuote(String ID) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void connect() throws Exception {
		market.connect();
		this.streamActive = true;
		this.run();
	}

	@Override
	public void disconnect() {
		this.streamActive = false;	
	}

}
