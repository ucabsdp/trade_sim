package com.trade_sim.market;
/**
 * Current types of market sources the system supports
 * @author Samuel Palmer
 *
 */
public enum MarketType {
CSV, YahooFinance
}
