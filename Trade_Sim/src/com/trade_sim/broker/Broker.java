package com.trade_sim.broker;

import java.util.PriorityQueue;

import com.trade_sim.asset.Position;
import com.trade_sim.framework.GLOBAL;
import com.trade_sim.framework.Run;
import com.trade_sim.market.MarketAccess;
import com.trade_sim.platform.Trade;

/**
 * A simple brokerage implementation placing trades direct to the market at the current quoted price.
 * It calculates simple transaction costs, either by percentage or fixed fee.
 * @author Samuel Palmer
 *
 */
public class Broker implements Brokerage {
	
	protected MarketAccess access;
	protected PriorityQueue<Trade> tradeQ = new PriorityQueue<Trade>();
	protected FeeType feeType;
	protected double feeValue;
	private String name = "Broker";
	
	
	
	public Broker(MarketAccess access, FeeType fee, double feeValue){
		this.access = access;
		this.feeType = fee;
		this.feeValue = feeValue;
	}
	
	public Broker(MarketAccess access){
		this.access = access;
		Run.log.log("Broker loaded using Market :" +access.toString());
	}

	@Override
	public Position placeTrade(Trade trade) {
		tradeQ.add(trade);
		return makeTrades();
	}
	
	protected Position makeTrades(){
		if(!tradeQ.isEmpty()){
			Trade t = tradeQ.poll();
			access.getQuote(t.getID());
		//	Run.log.log("Broker made trade " +t.toString());
			return Position.newPosition(t,GLOBAL.getTimeStep(), calculateCost(t));
		}else{
			return null;
		}
	}

	/**
	 * Calculates the one-way transaction cost of the given trade
	 * @param t
	 * @return
	 */
	private double calculateCost(Trade t){
		switch (this.feeType){
		case FIXED : return this.feeValue ;
		case PERCENTAGE : return (this.feeValue/100)*t.getQuantity()*t.getPrice();
		}
		return 0;
	}
	
	@Override
	public Position placeTrade() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String toString() {
		return "Broker [feeType=" + feeType + ", feeValue=" + feeValue
				+ ", name=" + name + "]";
	}

	@Override
	public void setMarketAccess(MarketAccess marketAccess) {
		this.access = marketAccess;
	}

	@Override
	public void setFee(FeeType type, double value) {
		this.feeType = type;
		this.feeValue = value;
		
	}
	
	

}
