package com.trade_sim.broker;

import com.trade_sim.asset.Position;
import com.trade_sim.market.MarketAccess;
import com.trade_sim.platform.Trade;


/**
 * Interface for a brokerage to be compatible with the system. A brokerage will place trades
 * onto the market, and return the opened positions.
 * @author Samuel Palmer
 *
 */
public interface Brokerage {
	public enum FeeType{
		PERCENTAGE, FIXED
	}
	
	public Position placeTrade();

	/**
	 * Places a trade onto the market and returns the corresponding position opened
	 * @param trade
	 * @return
	 */
	public Position placeTrade(Trade trade);

	/**
	 * Sets the market the brokerage should use for information
	 * @param marketAccess
	 */
	public void setMarketAccess(MarketAccess marketAccess);
	
	/**
	 * Set the fee of the broker
	 */
	public void setFee(FeeType type, double value);

}
