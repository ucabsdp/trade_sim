package com.trade_sim.broker;

import com.trade_sim.broker.Brokerage.FeeType;
import com.trade_sim.framework.Setting;


/**
 * Bean class containing configurable information about the broker
 * @author Samuel Palmer
 *
 */
public class BrokerSettings extends Setting{
	public FeeType feeType;
	public double feeValue;
	
	@Override
	public String toString() {
		return "BrokerSettings [feeType=" + feeType + ", feeValue=" + feeValue
				+ "]";
	}
	

}
