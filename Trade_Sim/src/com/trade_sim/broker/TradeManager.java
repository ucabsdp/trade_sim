package com.trade_sim.broker;

/**
 * CURRENTLY UNDER DEVELOPMENT
 * Will be used for more complex brokerage implementations where it will perform market timing and
 * executions at the best price.
 * @author Samuel Palmer
 *
 */
public interface TradeManager {
	
	public void openPosition();
	public void closePosition();
	public void cancelTrade();

}
