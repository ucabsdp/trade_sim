package com.trade_sim.bank;

import java.util.Observable;

import java.util.Observer;

import com.trade_sim.framework.GLOBAL;
import com.trade_sim.portfolio_mangement.PortfolioManager;


/**
 * Records the current balance, profit/loss of the user. Also provides calculations such as
 * the risk free rate of return on the starting balance
 * @author Samuel Palmer
 *
 */
public class Bank implements Observer{
	protected double riskFreeRate;
	protected double currentRiskFreeReturn;
	protected double startingValue;
	protected double currentValue;
	protected PortfolioManager portfolios;
	/**
	 * If the bank dynamically updates with time or only at the end of the simulation
	 */
	protected boolean isLive;
	
	
	public Bank(double startingValue, double riskFreeRate, boolean isLive){
		this.startingValue = startingValue;
		this.riskFreeRate = riskFreeRate;
		this.isLive = isLive ; 
		//If it is live then update at every new time interval that occurs
		//Otherwise manual updates can be triggered via currentRiskFreeReturn()
		if(isLive){
			GLOBAL.listener.addObserver(this);
		}
		
	}
	
	/**
	 * Reset the bank with a new starting value
	 * @param startValue
	 */
	public void reset(double startValue){
		this.startingValue = startValue;
		this.currentValue = startValue;
	}
	
	/**
	 * Invokes the calculation of the current risk free return manually. Uses {@link GLOBAL.getTimeElapsed()}
	 * @return Risk free value of the initial investment PV = IV*e^(r(T-t))
	 */
	public double currentRiskFreeReturn(){
		double r = riskFreeRate/GLOBAL.getTimeFrame().getValue();
		this.currentRiskFreeReturn = startingValue*Math.exp(r*GLOBAL.getTimeElapsed());
		return this.currentRiskFreeReturn;
	}

	@Override
	public void update(Observable arg0, Object arg1) {
		this.currentRiskFreeReturn();
	}
	
	
	/**
	 * 
	 * @return
	 */
	public double getRiskFreeRate() {
		return riskFreeRate;
	}
	
	/**
	 * Set the yearly risk free interest rate
	 * @param riskFreeRate
	 */
	public void setRiskFreeRate(double riskFreeRate) {
		this.riskFreeRate = riskFreeRate;
	}

	@Override
	public void finalize(){
		try {
			super.finalize();
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
