package com.trade_sim.bank;

import com.trade_sim.framework.Setting;

/**
 * Bean class holding configurable parameters for the bank class
 * @author Samuel Palmer
 *
 */
public class BankSettings extends Setting{
	
	public double riskFreeRate;
	public double startingValue;
	public boolean isLive;

}
